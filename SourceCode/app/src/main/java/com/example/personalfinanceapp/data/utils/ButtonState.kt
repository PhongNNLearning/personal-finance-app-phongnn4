package com.example.personalfinanceapp.data.utils

sealed class ButtonState() {
    object Clicked : ButtonState()
    object Loading : ButtonState()
    object Completed : ButtonState()
}
