package com.example.personalfinanceapp.data.model.user

import com.example.personalfinanceapp.data.model.bill.DailyBill
import com.example.personalfinanceapp.data.model.budget.CreditLimit

data class UserModel(
    var id: String? = null,
    var DOB: String? = null,
    var address: String? = null,
    var mobile: String? = null,
    var money: Long? = null,
    var name: String? = null,
    var dailyBills: ArrayList<DailyBill>? = null,
    var creditLimit: CreditLimit? = null,
) {

}