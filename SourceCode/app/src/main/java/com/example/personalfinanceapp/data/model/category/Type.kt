package com.example.personalfinanceapp.data.model.category

enum class Type() {
    FOOD, TRANSPORT, RENTING_HOUSE, WATER, PHONE, ELECTRIC, GAS, TV, INTERNET, ENTERTAINMENT, INCOME, OTHER
}