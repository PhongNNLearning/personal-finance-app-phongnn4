package com.example.personalfinanceapp.data.model.bill

data class FinancialFunction(
    var iconFunc: Int,
    var titleFunc: String
)