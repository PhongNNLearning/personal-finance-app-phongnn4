package com.example.personalfinanceapp.data.model.category

data class Category(
    val id: Int,
    val symbol: Int,
    val categoryName: String,
)