package com.example.personalfinanceapp.presentation.fragments.billsList

import com.example.personalfinanceapp.data.model.bill.Bill

interface OnBillListClicked {
    fun onClicked(bill: Bill)
}