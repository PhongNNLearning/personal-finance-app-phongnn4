package com.example.personalfinanceapp.presentation.fragments.payment

import com.example.personalfinanceapp.data.model.category.Category

interface OnCategoryListClicked {
    fun onItemClicked(category: Category)
}