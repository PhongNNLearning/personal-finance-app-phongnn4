package com.example.personalfinanceapp.presentation.fragments.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.personalfinanceapp.presentation.base.BaseViewModel
import com.example.personalfinanceapp.domain.usecases.accuracy.AccuracyUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val accuracyUseCase: AccuracyUseCase,
) : BaseViewModel() {

    private var _isLogged = MutableLiveData<Boolean>()
    val isLogged: LiveData<Boolean> get() = _isLogged

    fun checkLogged() {
        requestFlow {
            accuracyUseCase.isLogged().collect {
                _isLogged.postValue(it)
            }
        }
    }

}