package com.example.personalfinanceapp.data.model.budget

data class CreditLimit(
    val money: Long? = null,
    val note: String? = null,
    val type: String? = null,
    val startFrom: String? = null,
)