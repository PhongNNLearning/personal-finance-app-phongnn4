package com.example.personalfinanceapp.presentation.fragments.buget

import com.example.personalfinanceapp.data.model.category.Category

interface OnDateListClicked {
    fun onItemClicked(date: String)
}